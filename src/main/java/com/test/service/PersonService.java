package com.test.service;

import com.google.inject.ImplementedBy;
import com.test.model.Person;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by user on 01.05.2015..
 */
@ImplementedBy(PersonServiceImpl.class)
public interface PersonService {
    Stream<Person> findPersonByName(String name);
    Stream<Person> findAdults();
    Stream<Person> findPersons(Predicate<Person> personPredicate);
}
