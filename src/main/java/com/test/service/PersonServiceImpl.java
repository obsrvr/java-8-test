package com.test.service;

import com.test.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by user on 01.05.2015..
 */
public class PersonServiceImpl implements PersonService {

    private final List<Person> personList = Arrays.asList(
            new Person("John", 18, 10000L),
            new Person("Josh", 18, 10000L),
            new Person("Peter", 40, 14000L),
            new Person("Phil", 15, 16000L)
    );

    @Override
    public Stream<Person> findPersonByName(String name) {
        return personList.stream()
                .filter(person -> person.getName().equals(name));
    }

    @Override
    public Stream<Person> findAdults() {
        return personList.stream()
                .filter(person -> person.getAge() >= 18);
    }

    @Override
    public Stream<Person> findPersons(Predicate<Person> personPredicate) {
        return personList.stream()
                .filter(personPredicate);
    }
}
