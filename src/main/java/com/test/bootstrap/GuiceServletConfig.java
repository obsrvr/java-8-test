package com.test.bootstrap;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import javax.servlet.annotation.WebListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 02.05.2015..
 */
@WebListener
public class GuiceServletConfig extends GuiceServletContextListener {
    @Override
    protected Injector getInjector() {
        return Guice.createInjector(
                new JerseyServletModule() {
                    @Override
                    protected void configureServlets() {
                        Map<String, String> params = new HashMap<>();
                        params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
                        params.put("com.sun.jersey.config.property.packages", "com.test.endpoint");
                        serve("/*").with(GuiceContainer.class, params);
                    }
                }
        );
    }
}
