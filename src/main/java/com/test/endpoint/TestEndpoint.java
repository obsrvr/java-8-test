package com.test.endpoint;

import com.google.inject.Inject;
import com.test.model.Person;
import com.test.service.PersonService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 02.05.2015..
 */
@Path("/")
public class TestEndpoint {

    private final PersonService personService;

    @Inject
    public TestEndpoint(PersonService personService) {
        this.personService = personService;
    }

    @GET
    @Produces("application/json")
    public List<Person> getSignUp () {
        return personService.findAdults().collect(Collectors.toList());
    }
}
