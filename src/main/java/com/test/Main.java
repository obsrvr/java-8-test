package com.test;

import com.test.service.PersonService;
import com.test.service.PersonServiceImpl;

/**
 * Created by user on 01.05.2015..
 */
public class Main {

    public static void main(String ... args) {
        PersonService personService = new PersonServiceImpl();

        System.out.println("Custom search:");

        personService.findPersons(p -> p.getName().equals("John") && p.getAge() == 18)
                .forEach(System.out::println);

        System.out.println("Adults:");

        personService.findAdults()
                .forEach(System.out::println);

        System.out.println("Find by name:");

        personService.findPersonByName("Phil")
                .forEach(System.out::println);
    }
}
