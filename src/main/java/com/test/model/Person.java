package com.test.model;

/**
 * Created by user on 01.05.2015..
 */
public class Person {
    private String name;
    private Integer age;
    private Long salary;

    public Person(String name, Integer age, Long salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "(" + name + "), age=" + age + ", salary=" + salary;
    }
}
